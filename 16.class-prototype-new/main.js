//                              COMO FUNCIONAN LAS "CLASES" EN JAVASCRIPT
//

// ESTA ES LA NUEVA FORMA DE HERENCIA EN PROTOTIPOS
class Persona {
    constructor (nombre, apellido, altura){
        this.nombre = nombre
        this.apellido = apellido
        this.altura = altura
    }

    saludar (fn) {
        console.log(`Hola! Me llamo ${this.nombre} ${this.apellido}`)
        if (fn) {
            fn(this.nombre, this.apellido)
        }
    }

    soyAlto () {
        return this.altura > 1.8
    }
}


// PARA PODER LLAMAR A UN OBJETO DE ESTA CLASE DEBEMOS LLAMAR AL CONSTRUCTOR SUPER.
class Desarrollador extends Persona{
    constructor (nombre, apellido, altura) {
        super(nombre, apellido, altura)
    }

    saludar (fn) {
        console.log(`Hola soy ${this.nombre} ${this.apellido}, mido ${this.altura} y soy desarrollador/a.`)
        if (fn) {
            fn(this.nombre, this.apellido, true)
        }
    }
}


// FUNCIONES COMO PARÁMETROS

function responderSaludo (nombre, apellido, esDev) {
    if(esDev){
        console.log(`Genial ${nombre}! tambien eres desarrollador/a!`)
    }else{
        console.log(`Hola buen día, ${nombre} ${apellido}`)
    }
}

let fabian = new Desarrollador("Fabian", "Martinez", 1.60)
let Jeremy = new Persona("Jeremy", "One", 1.88)
let Jose = new Persona("Jose", "Gomez", 1.76)
let diana = new Desarrollador("Diana", "Montealba", 1.73)

fabian.saludar(responderSaludo)
Jeremy.saludar(responderSaludo)
Jose.saludar(responderSaludo)
diana.saludar(responderSaludo)
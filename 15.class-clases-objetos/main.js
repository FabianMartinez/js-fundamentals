//                              COMO FUNCIONAN LAS CLASES EN JAVASCRIPT
//
//  LOS OBJETOS EN JAVASCRIPT SON PROTOTIPOS


// HERENCIA DE PROTOTIPO
// ESTA ES UNA DE LAS PRIMERAS SOLUCIONES PARA CREAR HERENCIAS EN JAVASCRIPT
function HeredarDe (protoHijo, protoPadre) {
    let fn = function () {}
    fn.prototype = protoPadre.prototype
    protoHijo.prototype = new fn
    protoHijo.prototype.constructor = protoHijo
}


// ACÁ DEFINIMOS UN PROTOTIPO
// LA FUNCIÓN QUE DEFINE AL PROTOTIPO RETORNA IMPLÍCITAMENTE this, ES DECIR, RETORNA EL NUEVO OBJETO QUE SE CREÓ
// ESTA FUNCIÓN SE EJECUTARÁ CUANDO CREEMOS UNA NUEVA PERSONA
function Persona(nombre, apellido, altura){
    this.nombre = nombre
    this.apellido = apellido
    this.altura = altura
}
//  this HACE REFERENCIA A UN NUEVO OBJETO QUE SE ACABA DE CREAR


// EL PROTOTYPE NOS INDICA QUE ESTAMOS CREANDO UN PROTOTIPO DE PERSONA YA QUE USAREMOS SUS 
Persona.prototype.saludar = () => {
    console.log(`Hola! Me llamo ${this.nombre} ${this.apellido}`)
}

// ESTA FUNCIÓN CUMPLE UNA CONDICIÓN DE QUE SI, this.altura ES MAYOR A 1.8 ENTONCES IMPRIMIRÁ UN MENSAJE ACERCA DE SU ALTURA
Persona.prototype.soyAlto = () => {
    return this.altura > 1.8 
}

//  ESTE ES UN SUB-PROTOTIPO DE PERSONA
function Desarrollador (nombre, apellido) {
    this.nombre = nombre
    this.apellido = apellido
}

HeredarDe(Desarrollador, Persona)

Desarrollador.prototype.saludar = function () {
    console.log(`Hola soy ${this.nombre} ${this.apellido}, mido ${this.altura} y soy desarrollador.`)
}



// ESTAS SON LAS NUEVAS PERSONAS
// LOS ATRIBUTOS DE NOMBRE, APELLIDO Y ALTURA IRÁN DIRECTAMENTE A LOS PARÁMETROS DE LA FUNCIÓN Persona
/* let fabian = new Persona("Fabián", "Martínez", 1.76)
let jose = new Persona("José", "Rac", 1.86)
let diana = new Persona("Diana", "Gomez", 1.66) */
// LA PALABRA RESERVADA new SE UTILIZA PARA CREAR UN NUEVO OBJETO CON EL PROTOTIPO INDICADO

//  LLAMAMOS A LAS FUNCIONES DE saludar
/* fabian.saludar() 
jose.saludar()
diana.saludar() */

// LLAMAMOS A LAS FUNCIONES DE soyAlto
/* fabian.soyAlto()
jose.soyAlto()
diana.soyAlto() */
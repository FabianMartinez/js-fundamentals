//                               ARREGLANDO ASINCRONÍSMO DE JAVASCRIPT 

//  DEFINIMOS UNA CONSTANTE CON LA URL DE DONDE SACARÉMOS LOS DATOS
const API_URL = 'https://swapi.dev/api/'

//  ESTA ES "LA SEGUNDA PARTE" DE LA URL EN DONDE LE INDICAMOS A DONDE IREMOS A SACAR NUESTROS DATOS
const PEOPLE_URL = 'people/:id'
const PLANETS_URL = 'planets/:id'

//  ESTO NOS PERMITE HACER UNA REDIRECCIÓN SERVER-SIDE A OTRO DOMAIN
const OPTS = { crossDomain: true }

function obtenerPersonaje (id, callback) {
    const url = `${API_URL}${PEOPLE_URL.replace(':id', id)}`
    
    $.get(url, OPTS, callback).fail(function () {
        console.log(`NO SE PUDO OBTENER AL PERSONAJE ${id}`)
    })
}

obtenerPersonaje(1, function(personaje){
    console.log(`Hola yo soy ${personaje.name}`)

    obtenerPersonaje(2, function(personaje){
        console.log(`Hola yo soy ${personaje.name}`)

        obtenerPersonaje(3, function(personaje){
            console.log(`Hola yo soy ${personaje.name}`)

            obtenerPersonaje(4, function(personaje){
                console.log(`Hola yo soy ${personaje.name}`)

                obtenerPersonaje(5, function (personaje){
                    console.log(`Hola yo soy ${personaje.name}`)
                    
                })               
            })
        })
    })
})

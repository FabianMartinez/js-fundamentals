//                                    CALLBACK A LA API DE SWAPI.DEV 

//  DEFINIMOS UNA CONSTANTE CON LA URL DE DONDE SACARÉMOS LOS DATOS
const API_URL = 'https://swapi.dev/api/'

//  ESTA ES "LA SEGUNDA PARTE" DE LA URL EN DONDE LE INDICAMOS A DONDE IREMOS A SACAR NUESTROS DATOS
const PEOPLE_URL = 'people/:id'
const PLANETS_URL = 'planets/:id'

//  ESTO NOS PERMITE HACER UNA REDIRECCIÓN SERVER-SIDE A OTRO DOMAIN
const OPTS = { crossDomain: true }

const onPeopleResponse = function (persona) {
    console.log(`Hola yo soy ${persona.name}!`)
}

const onPlanetsResponse = function (planeta) {
    console.log(`Este es el planeta ${planeta.name}!`)
}

function obtenerPersonaje (id) {
    const url = `${API_URL}${PEOPLE_URL.replace(':id', id)}`
    $.get(url, OPTS, onPeopleResponse)
}

function obtenerPlaneta (id) {
    const url = `${API_URL}${PLANETS_URL.replace(':id', id)}`
    $.get(url, OPTS, onPlanetsResponse)
}

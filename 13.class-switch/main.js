alert("OPCIONES DE LA CALCULADORA \n1.Sumar \n2.Restar \n3.Multiplicar \n4.Dividir")
let optionCalc = prompt("Ingrese el número de su opción") 

let numA, numB, numAParsed, numBParsed

switch(optionCalc){

    case "1":
        numA = prompt("Ingrese el primer número de la suma: ")
        numB = prompt("Ingrese el segundo número de la suma: ")
        numAParsed = parseInt(numA)
        numBParsed = parseInt(numB)

        let opcionSuma = (a, b) => { return c = parseInt(a) + parseInt(b)}
        console.log(opcionSuma(numAParsed, numBParsed))
    break 

    case "2":
        numA = prompt("Ingrese el primer número de la resta: ")
        numB = prompt("Ingrese el segundo número de la resta: ")
        numAParsed = parseInt(numA)
        numBParsed = parseInt(numB)

        let opcionResta = (a, b) => { return c = parseInt(a) - parseInt(b)}
        console.log(opcionResta(numAParsed, numBParsed))
    break

    case "3":
        numA = prompt("Ingrese el primer número de la multiplicación: ")
        numB = prompt("Ingrese el segundo número de la multiplicación: ")
        numAParsed = parseInt(numA)
        numBParsed = parseInt(numB)

        let opcionMultiplicar = (a, b) => { return c = parseInt(a) * parseInt(b)}
        console.log(opcionMultiplicar(numAParsed, numBParsed))
    break

    case "4":
        numA = prompt("Ingrese el primer número de la división: ")
        numB = prompt("Ingrese el segundo número de la división: ")
        numAParsed = parseInt(numA)
        numBParsed = parseInt(numB)

        let opcionDivision = (a, b) => { return c = parseInt(a) / parseInt(b)}
        console.log(opcionDivision(numAParsed, numBParsed))
    break

    default:
        console.log("OPCIÓN INCORRECTA")
    
}
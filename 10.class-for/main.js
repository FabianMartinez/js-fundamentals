let person1 = {
    nombre : "Fabián",
    apellido : "Martínez",
    edad : 23,
    programmer : true,
    kg : 50
}

let person2 = {
    nombre : "Juan",
    apellido : "Gomez",
    edad : 17,
    programmer : false,
    kg : 65
}

console.log(`Al inicio del año ${person1.nombre} pesaba ${person1.kg}`)

const INCREMENTO = 0.2
const YEAR_DAYS = 365

let aumentoPeso = persona => persona.kg += INCREMENTO
let disminucionPeso = persona => persona.kg -= INCREMENTO

for(let i = 1; i <= YEAR_DAYS; i++){
    let random = Math.random()

    if(random < 0.25){
        aumentoPeso(person1)
    }else if(random < 0.5){
        disminucionPeso(person1)
    }
}

console.log(`Al fin de año ${person1.nombre} pesará ${person1.kg.toFixed(1)}`)
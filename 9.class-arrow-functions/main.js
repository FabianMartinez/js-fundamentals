let person1 = {
    nombre : "Fabián",
    apellido : "Martínez",
    edad : 23,
    programmer : true
}

let person2 = {
    nombre : "Juan",
    apellido : "Gomez",
    edad : 16,
    programmer : false
}

const MAYORIA_DE_EDAD = 18

const mayorEdad = ({edad}) => edad >= MAYORIA_DE_EDAD
const menorEdad = ({edad}) => edad <= MAYORIA_DE_EDAD

function confirmacionEdad(persona){
    if(mayorEdad(persona)){
        console.log(`${persona.nombre} es mayor de edad.`)
    }else{
        console.log(`${persona.nombre} no es mayor de edad.`)
    }
}

function permitirAcceso(persona){
    if(!mayorEdad(persona)){
        console.log("NO PUEDES REGISTRARTE EN ESTOS MOMENTOS.")
    }else{
        console.log("ACCESO PERMITIDO")
    }
}

function permisoMenores(persona){
    if(!menorEdad(persona)){
        console.log(`${persona.nombre} es mayor de edad, ACCESO DENEGADO.`)
    }
}

const permisoAccesoPublico = ({edad}) => !mayorEdad({edad}) ?

console.log("Acceso denegado") : console.log("Adelante")

permisoMenores(person1)
permisoMenores(person2)

confirmacionEdad(person1)
confirmacionEdad(person2)


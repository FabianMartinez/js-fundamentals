let contador = 0

let llueve = () => Math.random() < 0.25


do{
    contador++
}while(!llueve())

let frecuencia = contador === 1 ? "vez" : "veces";
console.log(`Fui a ver si llovía ${contador} ${frecuencia}.`)
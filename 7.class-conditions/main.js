var person1 = {
    nombre : "Fabián",
    apellido : "Martínez",
    edad : 23,
    programador : true,
    cocinero : false,
    cantante : false,
    escritor : false,
    emprendedor : true
}

function profesionPersona(persona){
    console.log(`${persona.nombre} es:`)

    if (persona.edad >= 18){
        console.log("Es mayor de edad, puedes ingresar.")
    }else{
        console.log("Eres menor de edad no puedes ingresar.")
    }

    if (persona.programador){
        console.log("Es un programador")
    }else{
        console.log("No es programador.")
    }

    if (persona.cocinero){
        console.log("Es cocinero")
    }else{
        console.log("No es cocinero.")
    }

    if (persona.cantante){
        console.log("Es cantante")
    }else{
        console.log("No es cantante.")
    }

    if (persona.escritor){
        console.log("Es escritor")
    }else{
        console.log("No es escritor.")
    }

    if (persona.emprendedor){
        console.log("Es emprendedor")
    }else{
        console.log("No es emprendedor.")
    }
}

profesionPersona(person1)


// CREAMOS NUESTRAS VARIABLES OBJETO 

// RECORDEMOS...
//  var variable = {
//      LLAVE : VALOR
//  }
//

var fabian = {
    nombre:'Fabián',
    apellido: 'Martínez',
    altura: 1.60,
    cantLibros : 183
}

var alan = {
    nombre: 'Alan',
    apellido: 'Perez',
    altura: 1.86,
    cantLibros: 34
}

var martin = {
    nombre: 'Martin',
    apellido: 'Gomez',
    altura: 1.85,
    cantLibros: 78
}

var dario = {
    nombre: 'Dario',
    apellido: 'Juarez',
    altura: 1.71,
    cantLibros: 129
}

var vicky = {
    nombre: 'Vicky',
    apellido: 'Zapata',
    altura: 1.56,
    cantLibros: 198
}

var paula = {
    nombre: 'Paula',
    apellido: 'Barros',
    altura: 1.76,
    cantLibros: 247
}


//                                 CONDICIONES PARA FILTRAR LOS OBJETOS                                    
//
//
//  ESTA FUNCIÓN NOS RETORNA A LAS PERSONAS ALTAS QUE MIDEN MAYOR O IGUAL A 1.8 METROS DE ALTURA.
const esAlto = persona => persona.altura >= 1.8 

//  ESTA FUNCIÓN NOS RETORNA A LAS PERSONAS BAJAS QUE MIDEN MENOR A 1.7 METROS DE ALTURA
const esBajo = persona => persona.altura < 1.7

//  ESTA FUNCIÓN NOS RETORNA A LAS PERSONAS DE ESTATURA MEDIA QUE MIDAN ENTRE 1.7 A 1.8 METROS DE ALTURA
const esMedio = persona => persona.altura < 1.8 && persona.altura > 1.7  



//  INICIALIZAMOS UN ARRAY COMO "PERSONAS" EN EL CÚAL GUARDAMOS NUESTRAS VARIABLES QUE SON NUESTROS
//  OBJETOS.

//  RECORDEMOS QUE LOS ARRAYS PUEDEN SER DE UN SOLO TIPO DE DATO. EN ESTE CASO, TENDREMOS UN ARRAY DE OBJETOS.
const personas = [fabian, alan, martin, dario, vicky, paula]





//                                     FILTRANDO DATOS DE UN ARRAY 

//  ESTA VARIABLE GUARDA UNA CONDICIÓN HECHA PREVIAMENTE (esAlto) PARA FILTRAR A LAS PERSONAS ALTAS DEL ARRAY.
let personasAltas = personas.filter(esAlto)
console.log("Personas de estatura alta", personasAltas)


//  ESTA VARIABLE GUARDA UNA CONDICIÓN HECHA PREVIAMENTE (esBajo) PARA FILTRAR A LAS PERSONAS BAJAS DEL ARRAY.
let personasBajas = personas.filter(esBajo)
console.log("Personas de estatura baja: ", personasBajas)


//  ESTA VARIABLE GUARDA UNA CONDICIÓN HECHA PREVIAMENTE (esMedio) PARA FILTRAR A LAS PERSONAS MEDIAS DEL ARRAY.
let personasAltMedia = personas.filter(esMedio)
console.log("Personas estatura media:", personasAltMedia)




//                         TRANSFORMAR LLAVE ALTURA DE METROS A CENTIMETROS


//CON ESTA FUNCIÓN PODREMOS CAMBIAR LA ALTURA DE LAS PERSONAS QUE NORMALMENTE ESTUVIERON DECLARADAS EN "METROS" PARA AHORA SER "CENTÍMETROS".
const mtsToCms = persona => {
    //persona.altura = persona.altura * 100 -- PARA NO MODIFICAR LOS OBJETOS ORIGINALES PODEMOS RETORNAR UN 
                                            // UN SPREAD OPERATOR DE PERSONA.
    return {
        ...persona,    // <--------------------ESTE ES UN SPREAD OPERATOR (...)
        altura: persona.altura * 100
    }
}
// DE ESTA FORMA TAMBIEN ES CORRECTA: 
// const mtsToCms = persona => persona.altura *= 100

// PRUEBA DE CODIGO 
const mtsToCms2 = persona => ({
    ...persona,
    altura: persona.altura * 100
})


// RESULTADO FINAL DE CONVERSIÓN DE METROS A CENTÍMETROS
let personasACms = personas.map(mtsToCms)
console.log("PERSONAS EN CENTÍMETROS:", personasACms)

// CÓDIGO DE PRUEBA
personasACms = personas.map(mtsToCms2)
console.log("PERSONAS EN CENTÍMETROS CON CÓDIGO 2:", personasACms)





//                                  UTILIZANDO REDUCER


// FUNCIÓN CONTADOR DE cantLibros DE CADA OBJETO
const reducer = (cont, persona) => cont + persona.cantLibros
// ALTERNATIVA
// const reducer = (cont, { cantLibros }) => cont + cantLibros

// USANDO REDUCE PARA CONTAR LA CANTIDAD DE ALGUN VALOR DE UN DETERMINADO ARRAY DE OBJETOS
let totalDeLibros = personas.reduce(reducer, 0)
console.log("EL TOTAL DE LIBROS DEL ARRAY ES:", totalDeLibros)



//  IMPRIMIMOS NUESTRO ARRAY DE OBJETOS
console.log(personas)




//                                 RECORRER DE UN ARRAY CON UN FOR

// PODEMOS RECORRER ESTE ARRAY CON UN FOR:

for(let i=0; i < personas.length;i++){
    let persona = personas[i]
    console.log(`DATOS USUARIO: ${persona.nombre} mide ${persona.altura}`)
}

let person1 = {
    nombre : "Fabián",
    apellido : "Martínez",
    edad : 23,
    programmer : true,
    kg : 50
}

let person2 = {
    nombre : "Juan",
    apellido : "Gomez",
    edad : 17,
    programmer : false,
    kg : 65
}

console.log(`Al inicio del año ${person1.nombre} pesaba ${person1.kg}`)

const INCREMENTO = 0.2
const YEAR_DAYS = 365

let aumentoPeso = persona => persona.kg += INCREMENTO
let disminucionPeso = persona => persona.kg -= INCREMENTO
let comerMucho = () => Math.random() < 0.3
let hacerDeporte = () => Math.random() < 0.4

let META = person1.kg - 3
let dias = 0

while (person1.kg > META){
    if(comerMucho()){
        aumentoPeso(person1)
    }

    if(hacerDeporte()){
        disminucionPeso(person1)
    }

    dias += 1
}

console.log(`Pasaron ${dias} dias hasta que ${person1.nombre} adelgazó 3kg`)
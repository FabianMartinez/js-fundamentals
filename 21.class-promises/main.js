//                                      PROMESAS EN JAVASCRIPT
//  ¿QUE SON LAS PROMESAS?
//  LAS PROMESAS SON VALORES QUE AÚN NO CONOCEMOS, ESTOS VALORES ESTARÁN PRESENTES CUANDO UNA ACCION        ASÍNCRONA SUCEDA Y SE RESUELVA

/* 
    LAS PROMESAS TIENEN 3 ESTADOS:

    1. PENDING 
    2. FULLFILLED
    3. REJECTED

    - CUANDO LA PROMESA SE RESUELVE EXITOSAMENTE:

        * PENDING ---> FULLFILLED  -----------> .then(=> value...)

    - O SI ESTE FRACASA:

        * PENDING ---> REJECTED  -----------> .catch(err =>...)


    TAMBIEN ES POSIBLE CREAR PROMESAS ENCADENADAS:

        * PENDING ---> FULLFILLED  -----------> PROMISE (PENDING) -----------> FULLFILLED
                                                       |
                                                     or...
                                                       |
                                                    rejected
                                                       |
                                                .catch(err=>...)
*/


//  DEFINIMOS UNA CONSTANTE CON LA URL DE DONDE SACARÉMOS LOS DATOS
const API_URL = 'https://swapi.dev/api/'

//  ESTA ES "LA SEGUNDA PARTE" DE LA URL EN DONDE LE INDICAMOS A DONDE IREMOS A SACAR NUESTROS DATOS
const PEOPLE_URL = 'people/:id'
const PLANETS_URL = 'planets/:id'

//  ESTO NOS PERMITE HACER UNA REDIRECCIÓN SERVER-SIDE A OTRO DOMAIN
const OPTS = { crossDomain: true }

function obtenerPersonaje (id) {
    return new Promise((resolve, reject) => {
        const url = `${API_URL}${PEOPLE_URL.replace(':id', id)}`
        $
        .get(url, OPTS, data => resolve(data))

        .fail(() => reject(id))
    })
}

function onError (id) {
    console.log(`Ha sucedido un error al obtener el personaje ${id}`)
}

obtenerPersonaje(1)
    .then( personaje => console.log(`El personaje 1 es ${personaje.name}`)
    )
    .catch(onError)
let person = {
    nombre : "Fabián",
    edad : 23,
    programmer : true
}

let person2 = {
    nombre : "Sacha",
    edad : 34,
    programmer : false
}

function personData(persona){
    // var nombre = persona.nombre
    var { nombre } = persona
    console.log(nombre.toUpperCase())
}

personData(person)
personData(person2)

let fabian = {
    nombre : "Fabián",
    edad : 23,
    programmer : true
}

let sacha = {
    nombre : "Sacha",
    edad : 34,
    programmer : false
}

function nombreEdad(persona){
    var {nombre} = persona
    var {edad} = persona
    console.log(`Hola soy ${nombre} y tengo ${edad}`)
}

nombreEdad(fabian)
nombreEdad(sacha)

//Valores por parametro y por referencia

function birthday(persona){
    persona.edad += 1
    console.log("Edad actual: ", persona.edad)
}

birthday(fabian)

function bdayAlter(persona){
    return {
        ...persona,
        edad: persona.edad + 1,
        nacimiento: "14 de Septiembre"
    }
}

bdayAlter(fabian)
